import Vue from 'vue'
import Router from 'vue-router'

const Index = r => require.ensure([], () => r(require('@/views/index')), 'Index');
const List = r => require.ensure([], () => r(require('@/views/list')), 'List');
const Detail = r => require.ensure([], () => r(require('@/views/detail')), 'detail');
const Lands = r => require.ensure([], () => r(require('@/views/lands')), 'lands');
const Download = r => require.ensure([], () => r(require('@/views/download')), 'download');
const Contract = r => require.ensure([], () => r(require('@/views/contract')), 'contract');
const LandDetail = r => require.ensure([], () => r(require('@/views/landDetail')), 'landDetail');



Vue.use(Router)

export default new Router({
    // mode:"history",
    routes: [
        {
            path: '/',
            redirect: '/index'
        },
        {
            path: '/index',
            name: 'Index',
            component: Index
        },
        {
            path: '/list',
            name: 'List',
            component: List
        },
        {
            path: '/detail',
            name: 'Detail',
            component: Detail
        },
        {
            path: '/lands',
            name: 'Lands',
            component: Lands
        },
        {
            path: '/downList',
            name: 'Download',
            component: Download
        },
        {
            path: '/landDetail',
            name: 'LandDetail',
            component: LandDetail
        },
        {
            path: '/contract',
            name: 'Contract',
            component: Contract
        }


    ]
})
