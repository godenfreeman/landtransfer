import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const store = new Vuex.Store({
	state: {
		current: window.localStorage.getItem('current') ? parseInt(window.localStorage.getItem('current')) : 0

	},
	mutations: {
		update(state, index) {
			state.current = index;
			window.localStorage.setItem('current', index);
		}
	}
})

export default{
	store
}