import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import './plugins/element.js'

import './assets/css/minireset.css'
import './assets/css/index.css'
import NavHeader from './components/header.vue'
import FooterBar from './components/footer.vue'

Vue.config.productionTip = false

Vue.prototype.$http=axios

// Vue.component('nav-header',NavHeader)
// Vue.component('footer-bar',FooterBar)



new Vue({
  render: h => h(App),
  router
}).$mount('#app')
